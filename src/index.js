const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://siko:1122334455@cluster0.zcwxfvk.mongodb.net/todoapp?retryWrites=true&w=majority');

const { usersRouter } = require('./usersRouter');
const { notesRouter } = require('./notesRouter');
const { authRouter } = require('./authRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res) {
  console.error('err');
  res.status(500).send({ message: 'Server error' });
}
app.use(errorHandler);
