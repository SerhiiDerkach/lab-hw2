const bcryptjs = require('bcryptjs');
const { User } = require('./models/Users');

const getProfileInfo = (req, res) => User.findById({ _id: req.user.userId }, '-__v -password')
  .then((user) => {
    res.status(200).json({ user });
  });
const deleteProfile = (req, res) => User.findByIdAndDelete({ _id: req.user.userId })
  .then(() => {
    res.status(200).json({ message: 'Successfully deleted' });
  });
const changeProfilePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const user = await User.findById({ _id: req.user.userId });
  const match = await bcryptjs.compare(String(oldPassword), String(user.password));
  const hashNewPass = await bcryptjs.hash(newPassword, 10);

  if (user && match) {
    await User.findByIdAndUpdate(req.user.userId, { password: hashNewPass });
    return res.status(200).json({ message: 'Password updated!' });
  }
  res.status(400).json({ message: 'User not found!' });
};

module.exports = {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
};
