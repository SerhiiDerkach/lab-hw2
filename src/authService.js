const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/Users');

const registerUser = async (req, res, next) => {
  const { name, username, password } = req.body;

  const user = new User({
    name,
    username,
    password: await bcryptjs.hash(password, 10),
  });

  user.save()
    .then(() => res.status(200).json({ message: 'Success registered' }))
    .catch((err) => {
      next(err);
    });
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, name: user.name, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.status(200).json({ message: 'success', jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};
