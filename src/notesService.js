const { Note } = require('./models/Notes');

const getUserNotes = (req, res) => {
  const { offset, limit } = req.params;
  return Note.find({ userId: req.user.userId }, '-__v')
    .then((notes) => {
      res.status(200).json({ offset, limit, notes });
    });
};
const addUserNotes = (req, res) => {
  const { text } = req.body;
  const { userId } = req.user;

  const note = new Note({
    userId,
    text,
  });
  note.save().then(() => {
    res.status(200).json({ message: 'Note Success added' });
  });
};

const getUserNoteById = (req, res) => Note.findById({ _id: req.params.id, userId: req.user.userId }, '-__v')
  .then((note) => {
    res.status(200).json({ note });
  });

const updateUserNoteById = async (req, res) => {
  const { text } = req.body;
  if (text) {
    return Note.findByIdAndUpdate(
      { _id: req.params.id, userId: req.user.userId },
      { $set: { text } },
    )
      .then((result) => {
        res.json({ message: 'Note was updated', result });
      });
  }
  return res.status(400).json({ message: 'Text not found' });
};

const checkUserNoteById = (req, res) => Note.findByIdAndUpdate(
  { _id: req.params.id, userId: req.user.userId },
  { $set: { completed: true } },
)
  .then(() => {
    res.json({ message: 'Note was marked completed' });
  });
const deleteUserNoteById = (req, res) => Note.findByIdAndDelete(
  { _id: req.params.id, userId: req.user.userId },
)
  .then(() => {
    res.status(200).json({ message: 'Note deleted successfully' });
  });

module.exports = {
  getUserNotes,
  addUserNotes,
  getUserNoteById,
  updateUserNoteById,
  checkUserNoteById,
  deleteUserNoteById,
};
