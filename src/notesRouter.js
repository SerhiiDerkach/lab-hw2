const express = require('express');

const router = express.Router();
const {
  addUserNotes, getUserNotes, getUserNoteById,
  updateUserNoteById, checkUserNoteById, deleteUserNoteById,
} = require('./notesService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/', authMiddleware, getUserNotes);

router.post('/', authMiddleware, addUserNotes);

router.get('/:id', authMiddleware, getUserNoteById);

router.put('/:id', authMiddleware, updateUserNoteById);

router.patch('/:id', authMiddleware, checkUserNoteById);

router.delete('/:id', authMiddleware, deleteUserNoteById);

module.exports = {
  notesRouter: router,
};
