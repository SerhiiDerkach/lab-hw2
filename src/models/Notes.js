const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
}, {
  timestamps: {
    createdAt: 'createdDate',
    updatedAt: false,
  },
});

const Note = mongoose.model('note', noteSchema);

module.exports = {
  Note,
};
