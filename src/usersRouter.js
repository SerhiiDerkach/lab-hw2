const express = require('express');

const router = express.Router();
const { getProfileInfo, deleteProfile, changeProfilePassword } = require('./usersService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.get('/me', authMiddleware, getProfileInfo);
router.delete('/me', authMiddleware, deleteProfile);
router.patch('/me', authMiddleware, changeProfilePassword);

module.exports = {
  usersRouter: router,
};
